# Very common error
def spam():
    print(eggs) # ERROR!
    eggs = 'spam local'

eggs = 'global'
spam()

#using 'global' makes it print the global eggs
'''
def spam():
    global eggs
    print(eggs) # ERROR!
    eggs = 'spam local'

eggs = 'global'
spam()
'''